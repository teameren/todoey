//
//  Category.swift
//  Todoey
//
//  Created by eren on 23.03.2019.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation
import RealmSwift

class Category: Object{
    
    @objc dynamic var name: String = ""
    @objc dynamic var colour: String = ""
    let items = List<Item>()
    
}
