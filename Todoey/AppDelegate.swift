//
//  AppDelegate.swift
//  Todoey
//
//  Created by eren on 13.01.2019.
//  Copyright © 2019 test. All rights reserved.
//

import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        do {
            _ = try Realm()
        }catch{
            print("realm init error in appdelegate \(error)")
        }
        
        return true
    }
}
